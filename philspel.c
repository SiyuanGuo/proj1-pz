/*
 * Include the provided hashtable library
 */
#include "hashtable.h"

/*
 * Include the header file
 */
#include "philspel.h"

/*
 * Standard IO and file routines
 */
#include <stdio.h>

/*
 * General utility routines (including malloc())
 */
#include <stdlib.h>

/*
 * Character utility routines.
 */
#include <ctype.h>

/*
 * String utility routines
 */
#include <string.h>

/*
 * this hashtable stores the dictionary
 */
HashTable *dictionary;

/*
 * the MAIN routine.  You can safely print debugging information
 * to standard error (stderr) and it will be ignored in the grading
 * process, in the same way which this does.
 */
int main(int argc, char **argv){
  



  if(argc != 2){
    fprintf(stderr, "Specify a dictionary\n");
    return 0;
  }
  /*
   * Allocate a hash table to store the dictionary
   */
  fprintf(stderr, "**********TESTingStart*********\n"); 
  //fprintf(stderr, "I Gto This%s, %s\n",argv[0],argv[1]);
  //fprintf(stderr, "%d\n", stringEquals("61b", "61c")); 
  
  //fprintf(stderr, "3 and %d\n",3);
//*********Test for q1********
  /*
  fprintf(stderr, "%d\n",stringHash("kbg"));
  fprintf(stderr, "%d\n",stringHash("hello katy"));
  fprintf(stderr, "%d\n",stringHash("hhhh"));
  fprintf(stderr, "%d\n",stringHash("hh"));
  fprintf(stderr, "%d\n",stringHash("hello kkkk"));
  fprintf(stderr, "%d\n",stringHash("hhhh"));
  */
//***********Test for q2*********
  /*fprintf(stderr, "%d\n",stringEquals("hello katy","puupup"));
  fprintf(stderr, "%d\n",stringEquals("hello katy","hello katy"));
  fprintf(stderr, "%d\n",stringEquals("hello ","hello katy"));
  fprintf(stderr, "%d\n",stringEquals("hello katy","kary"));
*/



  fprintf(stderr, "Creating hashtable\n");
  dictionary = createHashTable(2255, &stringHash, &stringEquals);

  fprintf(stderr, "Loading dictionary %s\n", argv[1]);
  readDictionary(argv[1]);
  //************Test for q3********
  /*
  int* i;
  int* j;
  int k = 1;
  //insertData(dictionary,"kkk",&k);

  i = findData(dictionary,"this");
  j = findData(dictionary,"weIrDw");
  
  fprintf(stderr,"go\n");
  fprintf(stderr,"go %d \n", i== NULL);
  fprintf(stderr,"go %d \n", j == NULL);

*/



  //************Test for q4********
  fprintf(stderr, "Dictionary loaded\n");

  fprintf(stderr, "Processing stdin\n");

  processInput();
  fprintf(stderr, "**********TESTingEnd*********\n"); 

  /* main in C should always return 0 as a way of telling
     whatever program invoked this that everything went OK
     */
  
  return 0;

}

/*
 * You need to define this function. void *s can be safely casted
 * to a char * (null terminated string) which is done for you here for
 * convenience.
 */
unsigned int stringHash(void *s){
  char *string = (char *) s;
  char *track = string;
  int output = 0;
  int i = 0;
  while (*track != NULL) {
    int numofcar = (int) *track;
    output += numofcar*7^(i);
    track = track+1;
    i = i+1;
  }
  
    

  //fprintf(stderr,"I Want This: %d\n", output);
  //fprintf(stderr,"%c\n", *(string));


  return output;
  fprintf(stderr,"Need to define stringHash\n");
  exit(0);
}

/*
 * You need to define this function.  It should return a nonzero
 * value if the two strings are identical (case sensitive comparison)
 * and 0 otherwise.
 */
int stringEquals(void *s1, void *s2){
  char *string1 = (char *) s1;
  char *string2 = (char *) s2;
  while (*string1!= NULL || *string2 != NULL) {
    //fprintf(stderr,"go %c %c\n", *string1, *string2);

    if (*string1 == NULL || *string2 == NULL) {
      //fprintf(stderr,"one\n");
      return 0;
    }
    if (*string1 != *string2) {
      //fprintf(stderr,"twp\n");
      return 0;
    }
    //fprintf(stderr,"go %c %c\n", *string1, *string2);
    string1 = string1+1;
    string2 = string2+1;
  }
  return 1;
  fprintf(stderr,"Need to define stringEquals\n");
  exit(0);
}

/*
 * this function should read in every word in the dictionary and
 * store it in the dictionary.  You should first open the file specified,
 * then read the words one at a time and insert them into the dictionary.
 * Once the file is read in completely, exit.  You will need to allocate
 * (using malloc()) space for each word.  As described in the specs, you
 * can initially assume that no word is longer than 60 characters.  However,
 * for the final 20% of your grade, you cannot assumed that words have a bounded
 * length You can NOT assume that the specified file exists.  If the file does
 * NOT exist, you should print some message to standard error and call exit(0)
 * to cleanly exit the program. Since the format is one word at a time, with
 * returns in between, you can
 * safely use fscanf() to read in the strings.
 */
void readDictionary(char *filename){
  int standard_word_size = 60*sizeof(char);
  //**********open file*******
  FILE* fp;
  //fprintf(stderr,"go %c \n", *(filename+1));
  fp = fopen(filename, "r");
  if (fp == NULL) {
    fprintf(stderr,"File does not exist\n");
    exit(0);
  }
//*******scan*********
  //fprintf(stderr,"1\n");
  int scansuccess;
  char* tempstr;
  tempstr = (char*) malloc(standard_word_size);
  scansuccess = fscanf (fp, "%s", tempstr);
  if (!scansuccess) {//no word in the file
    free(tempstr);
    fprintf(stderr,"No word in the file\n");
    return;
  }
  //********check if get a full word*************
  /*if (tempstr[59] != 0) {//the end of string is not 0, so the string is not colplete

  }
*/




  //fprintf(stderr,"%d\n", scansuccess);
  //*********continuing scan and put file in**********
  while (scansuccess!= -1) {
    //fprintf(stderr,"%d\n", scansuccess);
    insertData(dictionary,tempstr,&scansuccess);//maybe length of the string!?
    tempstr = (char*) malloc(60*sizeof(char));//give another space
    scansuccess = fscanf (fp, "%s", tempstr);//scan to the next thing
  }
  free(tempstr);
  //fscanf (fp, "%s", str);
  //fscanf (fp, "%s", str2);


  //fprintf(stderr,"2\n");

  /*fprintf(stderr,"go %s\n", str);
  fprintf(stderr,"go %s\n", str2);
*/
  /*
  int* i;
  int* j;
  int k = 1;
  insertData(dictionary,"kkk",&k);
  i = findData(dictionary,"kkk");
  j = findData(dictionary,"kjg");
  */
  /*
  fprintf(stderr,"go\n");
  fprintf(stderr,"go %d \n", *i);
  fprintf(stderr,"go %d \n", j != NULL);
*/
  return ;
  fprintf(stderr,"Need to define readDictionary\n");
  exit(0);
}


/*
 * This should process standard input and copy it to standard output
 * as specified in specs.  EG, if a standard dictionary was used
 * and the string "this is a taest of  this-proGram" was given to
 * standard input, the output to standard output (stdout) should be
 * "this is a teast [sic] of  this-proGram".  All words should be checked
 * against the dictionary as they are input, again with all but the first
 * letter converted to lowercase, and finally with all letters converted
 * to lowercase.  Only if all 3 cases are not in the dictionary should it
 * be reported as not being found, by appending " [sic]" after the
 * error.
 *
 * Since we care about preserving whitespace, and pass on all non alphabet
 * characters untouched, and with all non alphabet characters acting as
 * word breaks, scanf() is probably insufficent (since it only considers
 * whitespace as breaking strings), so you will probably have
 * to get characters from standard input one at a time.
 *
 * As stated in the specs, you can initially assume that no word is longer than
 * 60 characters, but you may have strings of non-alphabetic characters (eg,
 * numbers, punctuation) which are longer than 60 characters. For the final 20%
 * of your grade, you can no longer assume words have a bounded length.
 */
void processInput(){
  char Input;
  //char Output [60];
  char* chartoword;
  int sizeloc;
  char* chartoword_copy;
  //char* chartoword_alllower;

  //char name[60];
  //fprintf(stderr,"Here\n");

  //fprintf(stderr,"%d\n", stdin);
  //fprintf(stderr,"Here2\n");
  Input = getchar();//get first char

  //********in and out***********
  while (Input!= EOF) {//still have things as input
    //fprintf(stderr,"loop again \n");
    //to collect a whole word from calling a lot of char
    sizeloc = 60*sizeof(char);
    chartoword = (char*) malloc(sizeloc); //need to modify later
    //fprintf(stderr,"Here\n");
    //free(chartoword);
    //chartoword_copy = (char*) malloc(sizeof(chartoword));
    //chartoword_alllower = (char*) malloc(60*sizeof(char));
    int i = 0;
    //chartoword[2] = 'z';//testing
    //fprintf(stderr,"char to word is %s \n", chartoword);
    while ((Input>='a'&& Input<='z') || (Input>='A' && Input<='Z')) {//input is a char we need to add to chartoword
      //fprintf(stderr,"get this %c \n", Input);
      /*
      if (Input == "\n") {
        return;
      }
      */
      fprintf(stderr,"char to word: %c\n", Input);
      chartoword[i] = Input;//add the char to chartoword
      Input = getchar();//get another Input
      i+=1;
    }
    chartoword[i] = '\0';//add a NUL terminator
    fprintf(stderr,"char to word2: %s\n", chartoword);
    
    //fprintf(stderr,"word %s \n", chartoword);
    //*********process the wrd we get*************
    //get chartoword_2lower
    /*
    int j = 1;

    while( chartoword[j] ) 
    {
      chartoword_2lower[j]=tolower(chartoword[j]));
      i++;
    }
*/

    if (i!= 0) {//we actually increment i, which mean we get a word to check in dictionary
      //fprintf(stderr,"loop again2 \n");
      //make a copy of original
      chartoword_copy  = (char*) malloc(sizeloc); ;
      int k = 0;
      while (chartoword[k]!= 0) {
        *(chartoword_copy+k) = *(chartoword+k);
        k+=1;
      }

      if (findData(dictionary,chartoword_copy)!= NULL) {//self in dictionary?
        fprintf(stdout,"%s", chartoword);
      } else {
          int j = 1;
          while( chartoword_copy[j] ) 
            {

              chartoword_copy[j]=tolower(chartoword_copy[j]);//make old char to lower
              j++;
            }
            if (findData(dictionary,chartoword_copy)!= NULL) {// new char to word, lower case after 1st element, check dict
              fprintf(stdout,"%s", chartoword);
            } else {
              chartoword_copy[0] = tolower(chartoword_copy[0]);
              if (findData(dictionary,chartoword_copy)!= NULL) {// new char to word, lower case , check dict
                fprintf(stdout,"%s", chartoword);
              } else {
                fprintf(stdout,"%s [sic]", chartoword);//not found in dic, add [sic to the end]
              }
            }
      }//finish checking all three form of words
      int l = 0;
      while(chartoword[l]!=0){//clean up memory
        chartoword_copy[l]=0;
        l++;
      }
      free(chartoword_copy);
      chartoword_copy = NULL;
    }
    //after checking word, output the symbol that separates the words
    //gets(Input);
    //fprintf(stderr,"Here3\n");
    if (Input==EOF) {
      return;
    }
    fprintf(stdout,"%c", Input);
    Input = getchar(); //fgets(Input,60,stdin);
//free used things
    //fprintf(stderr,"char to word is enpty2%s \n", chartoword);
    //fprintf(stderr,"Here2\n");
    int m = 0;
    while (chartoword[m]!=0){//clean up memory
      chartoword[m]=0;
      m++;
    }
    free(chartoword);
    chartoword = NULL;
      
    //fprintf(stderr,"char to word is enpty2%s \n", chartoword);

    //fprintf(stderr,"%c \n", *Input);

  }
  

  /*
  fscanf (stdin, "%c", Input);  //fgets(Input,60,stdin);
  //gets(Input);
  fprintf(stderr,"Here3\n");
  fprintf(stdout,"%c\n", Input);
*/

  //Output = Input;

  //fprintf(stderr,"Here4\n");

  //printf("this is this %d",*Output);
  
  return;
  fprintf(stderr,"Need to define processInput\n");
  exit(0);
}
